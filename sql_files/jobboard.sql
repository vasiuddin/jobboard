-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2020 at 04:31 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `jobnum` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(8000) NOT NULL,
  `date_posted` datetime DEFAULT NULL,
  `date_ending` datetime DEFAULT NULL,
  `salary` varchar(20) DEFAULT NULL,
  `empType` varchar(20) DEFAULT NULL,
  `numopenings` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `jobnum`, `user_id`, `title`, `description`, `date_posted`, `date_ending`, `salary`, `empType`, `numopenings`) VALUES
(1, 788603, 2, 'software engineer', '<p>second</p>\r\n', NULL, NULL, '100000', 'Full Time', 1),
(2, 288993, 2, 'software engineer', '<p>second</p>\r\n', NULL, NULL, '100000', 'Full Time', 1),
(3, 530933, 2, 'software engineer', '<p>first&nbsp; edited</p>\r\n', NULL, NULL, '100000', 'Full Time', 1),
(4, 548450, 2, 'software engineer', '<p>assfasfasd</p>\r\n', NULL, NULL, '100000', 'Full Time', 1),
(5, 708541, 2, 'software engineer', '<p>afgsfsgdfg sdf g sdfgsdfgsdfg</p>\r\n', NULL, NULL, '100000', 'Full Time', 1),
(6, 613109, 2, 'software engineer', '<p>afgsfsgdfg sdf g sdfgsdfgsdfg</p>\r\n', NULL, NULL, '100000', 'Full Time', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `usertype` varchar(225) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `password_reset_token` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `city`, `phone`, `email`, `usertype`, `username`, `password`, `register_date`, `password_reset_token`) VALUES
(2, 'vasi', 'tolichowki', 'hyderabad', '9676201443', 'vasi_eagle@yahoo.com', 'Employeer', 'vasiuddin', '$2y$10$ph3LnSqeCiUlom/tD6mNNewjWoh9RroV9c/mXZIU0KXtBPuxQt98q', '2020-05-06 12:37:53', NULL),
(3, 'seeker', 'tolichowki', 'hyderabad', '9676201443', 'vasi2@email.com', 'Employeer', 'vasiuddin2', '$2y$10$6iNDjvEiPNkrrBP6tjY/muSXIbS.ZniRvoLkvCvHIt2YqAhP0jWkq', '2020-05-06 12:37:58', NULL),
(4, 'seeker2', 'tolichowki', 'hyderabad', '9676201443', 'vasi3@email.com', 'Job Seeker', 'vasiuddin3', '$2y$10$bOa.UbQN/Zpcszdu7pQTCuaVlcMQ7flBQKRmT4VvvWbgwBn3WL8/a', '2020-05-06 12:27:27', NULL),
(5, 'admin', 'tolichowki', 'hyderabad', '9676201443', 'admin@email.com', 'Admin', 'admin', '$2y$10$jv41iDVDaIsukWOkuJ4RIuQw05mqVspc4bvgeQmTGyGJiXJZl0.mS', '2020-05-06 13:06:49', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
