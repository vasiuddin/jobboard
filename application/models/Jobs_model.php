<?php

class Jobs_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }
	
    //

    public function get_jobs($jobnum= FALSE, $limit = 0, $offset = 0, $where = "")
    {
        if($limit)
        {
            $this->db->limit($limit, $offset);
        }
        
        $this->db->order_by('jobs.id', 'DESC');
        
		
		
        if($jobnum==FALSE && empty($where))
        {
			
            $query = $this->db->get('jobs');
            return $query->result_array();
        }
		else if(!empty($where))
		{
			
			$query = $this->db->where($where['field'],$where['value']);
			$query = $this->db->get('jobs');
            return $query->result_array();
		}
        else
        {
			
            $query = $this->db->get_where('jobs', array('jobnum'=>$jobnum));
            return $query->row_array();
        }
        
        return false;
    }
	
    //
    public function create($jobnum)
    {
        $data= array(
            'jobnum'=>$jobnum,
            'user_id'=>  $this->session->userdata('user_id'),
            'title'=>$this->input->post('title'),
            'description'=>  $this->input->post('desc'),
            'salary'=>$this->input->post('salary'),
            'emptype'=>  $this->input->post('emptype'),
            'numopenings'=>  $this->input->post('openings'),
        );
        $this->security->xss_clean($data);
        $this->db->insert('jobs', $data);
    }
	
    //
    public function update($jobnum)
    {
        $data= array(
            'title'=>$this->input->post('title'),
			'date_ending'=> $date_ending,
            'description'=>  $this->input->post('desc'),
            'salary'=>$this->input->post('salary'),
            'emptype'=>  $this->input->post('emptype'),
            'numopenings'=>  $this->input->post('openings'),
        );
		$this->security->xss_clean($data);
        $this->db->where('jobnum', $jobnum);
        $this->db->update('jobs', $data);
    }
	
	
	
	
   //
    public function get_poster($jobnum)
    {
        $query = $this->db->get_where('jobs', array('jobnum'=>$jobnum));

        return $query->row_array()['user_id'];
        
    }
	

    //
    public function is_job_num_taken($jobnum)
    {
        $query = $this->db->get_where('jobs', array('jobnum'=>$jobnum));
        if(empty($query->row_array()))
        {
                return false;
        }
        else{
                return true;
        }
    }
	
    //  
    public function get_count_rows_jobs()
	{
	
		return $this->db->get('jobs')->num_rows();
		
	}
	
}