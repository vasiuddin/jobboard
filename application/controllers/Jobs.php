<?php
class Jobs extends CI_Controller{
    
    //
    public function index($offset=0){

        $config['base_url'] = base_url().'jobs/index';
        $config['total_rows'] = $this->jobs_model->get_count_rows_jobs();
        $config['per_page'] = 3;
        $config['uri_segment'] = 3;
        $config['attributes'] = array('class' => 'pagination-link');
        $this->pagination->initialize($config);
        $data['jobs'] = $this->jobs_model->get_jobs($jobnum= FALSE, $config['per_page'], $offset);
        // print_r($data['jobs']); die;

        $data['title'] = 'Jobs Listing';
		
        
        if(!empty($data['jobs']))
        {
            $data['nojobs'] = "";
        }
        else
        {
            $data['nojobs'] = "No jobs available";
        }
        
        
        $this->load->view('templates/header');
        $this->load->view('jobs/index',$data);
        $this->load->view('templates/footer');
    }
    
    //
    public function view($jobnum)
    {
        $data['title'] = 'Job #'.$jobnum;
        
        $data['job'] = $this->jobs_model->get_jobs($jobnum); 

        
        $this->load->view('templates/header');
        $this->load->view('jobs/view',$data);
        $this->load->view('templates/footer');
    }
    
    //
    public function create()
    {

        
        if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
        $userid= $this->session->userdata('user_id');
        $currUser = $this->user_model->get_userinfo($userid);
        if($currUser['usertype'] == "Employeer"){


        
            $data['title'] = 'Create Job Posting';
            
            $this->form_validation->set_rules('title', 'Title', 'required|max_length[255]');
            $this->form_validation->set_rules('desc', 'Description', 'required');
            $this->form_validation->set_rules('openings', 'Number of Openings', 'required|is_natural_no_zero');
            
            if($this->form_validation->run() == FALSE){
                $this->load->view('templates/header');
                $this->load->view('jobs/create',$data);
                $this->load->view('templates/footer');
            }
            else
            {
                $dateposted = date("o-m-d",strtotime("now"));
    			
    			$jobnum_taken = true;
    			
                while($jobnum_taken)
                {
    				$jobnum = rand( 111111, 999999);
                    $jobnum_taken = $this->jobs_model->is_job_num_taken($jobnum);
                }

                $this->jobs_model->create($jobnum);
                $this->session->set_flashdata('job_created','Your job posting has been successfully created.');
                redirect('jobs/'.$jobnum);
            }
        }else
        {
           show_404();
        }
    }


    //
    public function edit($jobnum)
    {
        $userid = $this->jobs_model->get_poster($jobnum);
        if(!$this->session->userdata('logged_in') && $userid == $this->session->userdata('user_id')){
            redirect('users/login');
        }
        
        $data['title'] = 'Edit Job Posting #'.$jobnum;
        $data['job'] = $this->jobs_model->get_jobs($jobnum); 
        
        if(empty($data['job']))
        {
            show_404();
        }
        
        
        $this->load->view('templates/header');
        $this->load->view('jobs/edit',$data);
        $this->load->view('templates/footer');
    }
    
    //
    public function update($jobnum)
    {
        if(!$this->jobs_model->get_poster($jobnum) == $this->session->userdata('user_id')){
            redirect('users/login');
        }
        
        $this->form_validation->set_rules('title', 'Title', 'required|max_length[255]');
        $this->form_validation->set_rules('desc', 'Description', 'required');
        $this->form_validation->set_rules('openings', 'Number of Openings', 'required|is_natural_no_zero');
        
        if($this->form_validation->run() == FALSE){
            $data['title'] = 'Edit Job Posting #'.$jobnum;
			
			
            $data['job'] = $this->jobs_model->get_jobs($jobnum); 

            $this->load->view('templates/header');
            $this->load->view('jobs/edit',$data);
            $this->load->view('templates/footer');
        }
        else
        {
			

			
            $this->jobs_model->update($jobnum);
			$this->session->set_flashdata('job_updated','The job posting has been successfully updated.');
            redirect('jobs/'.$jobnum);
        }
        
  
        
        
    }

    //
    public function query($field, $value, $offset=0)
    {
        
        if($field != "occupation" && $field != "location" )
        {
			$this->session->set_flashdata('job_category_does_not_exist','Sorry but the cateogry you are trying to search for does not exist.');
            redirect('jobs/search');
        }
        
        $value = str_replace('%20'," ",$value);

        if($field == "occupation")
        {
            $valID = $this->jobs_model->get_occupation_byname(ucwords($value))['id'];
			if(!$valID) $valID = 0;
        }
        if($field == "location")
        {
            $valID = $this->jobs_model->get_location_byname(ucwords($value))['id'];
			if(!$valID) $valID = 0;
        }

		if( $valID == 0)
		{
			$this->session->set_flashdata('job_category_does_not_exist','Sorry but the sub-cateogry you are trying to search for does not exist.');
            redirect('jobs/search');
		}
		
        //$data['jobs'] = $this->jobs_model->get_jobs_filter($field, $valID);
		
		$config['base_url'] = base_url().'jobs/search/'.$field.'/'.$value;
		$config['total_rows'] = $this->jobs_model->get_count_rows_jobs($field, $valID);
		$config['per_page'] = 3;
		$config['uri_segment'] = 5;
		$config['attributes'] = array('class' => 'pagination-link');
		
        $data['jobs'] = $this->jobs_model->get_jobs($jobnum= FALSE, $config['per_page'],$offset,array('field'=>$field, 'value'=>$valID) );
        
        if(!empty($data['jobs']))
        {
			$this->pagination->initialize($config);
			$data['title'] = 'Jobs for ' . str_replace("_"," ",$value);
            
			$this->load->view('templates/header');
            $this->load->view('jobs/index',$data);
            $this->load->view('templates/footer');
        }
        else
        {
			if($field == "occupation")
			{
				$data['title'] = "No jobs available in the category of " . ucwords(str_replace("_","/",$value));
			}else if($field == "location"){
				$data['title'] = "No jobs located in " . ucwords(str_replace("_","/",$value));
			}
            
            
            $this->load->view('templates/header');
            $this->load->view('jobs/index',$data);
            $this->load->view('templates/footer');
        }
        
        
        
    }
	
}



