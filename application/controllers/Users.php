<?php
class Users extends CI_Controller{
	
	//
	public function index()
	{
		//echo "hi"; die;
		if(!$this->session->userdata('user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";
		$this->load->view('templates/header');
        $this->load->view('users/index', $data);
        $this->load->view('templates/footer');
	}

	//
	public function userview($id)
	{
		// echo "$id"; die;
		
		$currUser = $this->user_model->get_userinfo($id);
		// print_r($currUser); die;

		$data['user'] = $currUser;
		$data['title'] = "User Account";
		$this->load->view('templates/header');
        $this->load->view('users/index', $data);
        $this->load->view('templates/footer');
	}


	//
    public function register(){
        
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('addr', 'Address', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('usertype', 'Usertype', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'required|matches[password]');
        if($this->form_validation->run() === FALSE){
            
            $this->load->view('templates/header');
            $this->load->view('users/register', $data);
            $this->load->view('templates/footer');
        }
        else{
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            //$enc_password = md5($this->input->post('password'));
            $this->user_model->register($enc_password);

            $this->session->set_flashdata('user_registered', 'You are now registered and can log in');
            redirect('users/login');
            
        }
        
         
    }
    
    //
    public function login(){
		if($this->session->userdata('logged_in')){
            redirect('jobs');
        }
		
        $data['title'] = 'Sign In';
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/header');
            $this->load->view('users/login', $data);
            $this->load->view('templates/footer');
        } 
        else {
            $username = $this->input->post('username');
			$password = $this->input->post('password');
			
            $user_id = $this->user_model->login($username, $password);
            if($user_id){
                $user_data = array(
                    'user_id' => $user_id,
                    'username' => $username,
                    'logged_in' => true
                );
                $this->session->set_userdata($user_data);
				
				
				//set cookie for 1 year
				$cookie = array(
					'name'   => 'jobboard_user_id',
					'value'  => $user_id,
					'expire' => time()+31556926
				);
				$this->input->set_cookie($cookie);

				
                $this->session->set_flashdata('user_loggedin', 'You are now logged in');
                redirect('jobs');
            } 
            else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('users/login');
            }		
        }
    }

    
    public function logout(){

        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
		
		delete_cookie('jobboard_user_id');

        $this->session->set_flashdata('user_loggedout', 'You are now logged out');
        redirect('users/login');
    }
    
	
	
	
	
	
	//
    public function edit($user_id)
    {

        if(!$this->session->userdata('logged_in') && $userid == $this->session->userdata('user_id')){
            redirect('users/login');
        }
		$data['title'] = 'Edit User Account';
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('addr', 'Address', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|callback_checkpwdmatch');

		if($this->form_validation->run() === FALSE)
		{

			$data['user'] = $this->user_model->get_userinfo($user_id); 
			if(empty($data['user']))
			{
				show_404();
			}
			else
			{
				$this->load->view('templates/header');
				$this->load->view('users/edit', $data);
				$this->load->view('templates/footer');
			}
            
        }
        else
		{
            $this->user_model->update($user_id);

            $this->session->set_flashdata('user_updated', 'Your account is now updated.');
            redirect('users');
            
        }
        
    }
	

	 

	//
	public function checkpwdmatch($password)
	{
		
		$this->db->where('id', $this->session->userdata('user_id'));
        $result = $this->db->get('users');
		
        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message('checkpwdmatch', 'Incorrect password');
				return FALSE;
			}
            
			
			
        } 
		
		$this->form_validation->set_message('checkpwdmatch', 'The password field can not be empty');
		return FALSE;
        
	}
	
	//
	public function check_username_exists_or_unique($username)
	{

		$user = $this->session->userdata('username');
		if($user != $username)
		{
			$this->db->where('username', $username);
			$result = $this->db->get('users');
			
			if($result->row(0))
			{
				$this->form_validation->set_message('check_username_exists_or_unique', 'This username is taken by someone else');
				return FALSE;
			}
			return TRUE;
		
		}
		return TRUE;
        
	}
	
	
	//
	public function check_email_exists_or_unique($email)
	{

		$user = $this->session->userdata('email');
		if($user != $email)
		{
			$this->db->where('email', $email);
			$result = $this->db->get('users');
			
			if($result->row(0))
			{
				$this->form_validation->set_message('check_email_exists_or_unique', 'This email is taken by someone else');
				return FALSE;
			}
			return TRUE;
		
		}
		return TRUE;
        
	}

	//
	public function admin()
	{
		$userid= $this->session->userdata('user_id');
        $currUser = $this->user_model->get_userinfo($userid);
        if($currUser['usertype'] == "Admin"){

        $data['jobs'] = $this->jobs_model->get_jobs();
        $data['users'] = $this->user_model->get_users();


        	$this->load->view('templates/header');
        	$this->load->view('users/admin',$data);
        	$this->load->view('templates/footer');
        }else{
        	show_404();
        }
	}
	
}
