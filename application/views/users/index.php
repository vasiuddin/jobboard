<?php if($this->session->flashdata('user_updated')): ?>
    <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_updated').'</p>'; ?>
<?php endif; ?>

<h2><?= $title; ?></h2>

<p><b>Business Name:</b> <?php echo $user["name"]?> </p>
<p><b>Address:</b> <?php echo $user["address"]?> </p>
<p><b>City:</b> <?php echo $user["city"]?> </p>
<p><b>Phone</b> <?php echo $user["phone"]?> </p>
<p><b>Email:</b> <?php echo $user["email"]?> </p>

<p><b>User Name:</b> <?php echo $user["username"]?> </p>
<p><b>User Type:</b> <?php echo $user["usertype"]?> </p>

<p><b>Date Registered:</b> <?php echo  date('F j, Y',strtotime($user["register_date"]));?> </p>

<p><a class='btn btn-default pull-left' href='<?php echo base_url().'users/edit/'.$user["id"]?>'>Edit</a></p>


<script>

</script>