<?php if($this->session->flashdata('job_deleted')): ?>
    <?php echo '<p class="alert alert-success">'.$this->session->flashdata('job_deleted').'</p>'; ?>
<?php endif; ?>

<h2 class="jobListingTitle"><?php echo "Jobs" ?></h2>
<?php 
if(!$jobs)
{
	echo "No jobs have been posted";
}
?>
<?php foreach ($jobs as $job): ?>

    <?php 
    ?>
	<div class="jobListing">
		<h3 class="joblink"><a href="<?php echo base_url()."jobs/".$job['jobnum']; ?>"><span><?php echo $job['title'];?></a></span></h3>
		<p class="name">description: <span><?php echo $job['description'];?></span></p>

		<hr>
	</div>
<?php endforeach; ?>

<h2 class="UserListingTitle"><?php echo "Users" ?></h2>
<?php 
if(!$users)
{
	echo "No users have been posted";
}
?>
<?php foreach ($users as $user): ?>

    <?php 
    ?>
	<div class="jobListing">
		<h3 class="userlink"><a href="<?php echo base_url()."users/".$user['id']; ?>"><span><?php echo $user['username'];?></a></span></h3>
				<p class="name">User Type: <span><?php echo $user['usertype'];?></span></p>

		<hr>
	</div>
<?php endforeach; ?>
<div class='pagination-links'>
    <?php echo $this->pagination->create_links(); ?>
</div>
