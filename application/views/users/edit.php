<h2><?= $title; ?></h2>


<?php echo form_open('users/edit/'.$user["id"]); ?>
    <div class="form-group">
        <label>Business Name</label>
        <input type="text" class="form-control" name="name" placeholder="name" value='<?php echo $user['name']; ?>'/>
        <?php echo form_error('name','<span class="error">', '</span>'); ?>
    </div>
	<div class="form-group">
        <label>Address</label>
        <input type="text" class="form-control" name="addr" placeholder="address" value="<?php echo $user['address']; ?>"/>
        <?php echo form_error('addr','<span class="error">', '</span>'); ?>
    </div>
	<div class="form-group">
        <label>City</label>
        <input type="text" class="form-control" name="city" placeholder="city" value="<?php echo $user['city']; ?>"/>
        <?php echo form_error('city','<span class="error">', '</span>'); ?>
    </div>
	
	<div class="form-group">
        <label>Phone</label>
        <input type="text" class="form-control" name="phone" placeholder="111-111-1111" value="<?php echo $user['phone']; ?>"/>
        <?php echo form_error('phone','<span class="error">', '</span>'); ?>
    </div>

    <div class="form-group">
        <label>Username</label>
        <input type="text" class="form-control" name="username" placeholder="username" value="<?php echo $user['username']; ?>"/>
        <?php echo form_error('username','<span class="error">', '</span>'); ?>
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" name="email" placeholder="email" value="<?php echo $user['email']; ?>"/>
        <?php echo form_error('email','<span class="error">', '</span>'); ?>
    </div>
    
    <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control" name="password" placeholder="password" />
        <?php echo form_error('password','<span class="error">', '</span>'); ?>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
<?php echo form_close(); ?>