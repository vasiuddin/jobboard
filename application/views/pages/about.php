<h2><?= $title ?></h2>
<h3>List of Features</h3>
<ul>
	<li>Create Job</li>
	<li>Edit Job</li>
	<li>Delete Job</li>
	<li>Using CodeIgniter Security class add XSS and CSRF</li>
	<li>User Registration/Login</li>
	<li>Contact Page</li>
	<li>Pagination For Jobs </li>
	<li>User Account Page</li>
	<li>Delete account and all posts</li>
</ul>
